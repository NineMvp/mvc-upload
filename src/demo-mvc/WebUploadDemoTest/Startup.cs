﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebUploadDemoTest.Startup))]
namespace WebUploadDemoTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
