﻿using System.Web;
using System.Web.Optimization;

namespace WebUploadDemoTest
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/browser.detect.js"));

            //bundles.Add(new ScriptBundle("~/bundles/angular").Include("~/Scripts/angular.js"));

            //~/Scripts/kendo.core.min.2013.1.319.js
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendocore").Include("~/Scripts/kendo.core.min.2013.1.319.js"));
            bundles.Add(new ScriptBundle("~/bundles/plupload").Include("~/Scripts/plupload/plupload.full.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/XPhotoUpload").Include("~/Scripts/XPhotoUpload.js"));


            bundles.Add(new StyleBundle("~/Content/xphotoupload").Include("~/Content/xphotoupload.css"));
            bundles.Add(new StyleBundle("~/Content/bootstrap/base").Include("~/Content/bootstrap/bootstrap.css"));
            bundles.Add(new StyleBundle("~/Content/bootstrap/theme").Include("~/Content/bootstrap/bootstrap-theme.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/plupload/",
                "~/Content/site.css"));

        }
    }
}
