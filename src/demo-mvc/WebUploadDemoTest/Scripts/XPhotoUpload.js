﻿/* 
  license : LPGPv2 
  by: chalermpon areepong
  email: ninemvp@gmail.com
  date: 2013-11-01
*/
function XPhotoUpload(elm) {
    /* variables */
    /* pluploader instance */
    var uploaderInstance;
    /* upload element */
    var caller;
    /* upload option parameters */
    var allowExts, sizeLimit, showConsole, uploadRuntime,
        urlUpload, chunkSize, flashPath, silverlightPath, showPreview = false,
        previewHeight = 150, multiUpload = false, isAutoUpload = false, isClearLastUpload = false;
    /* get function is support thumbnail and preview */
    var _URL = window.URL || window.webkitURL;
    /* upload UI templates */
    var tmplUploadTable = '<div class="table-responsive"><table id="tbupload" class="table table-hover"><thead><tr><th class="no-thumbnail">Photo</th><th>File Name</th><th>File Size</th><th class="no-thumbnail">Dimension</th><th>Status</th><th>Action</th></tr></thead><tfoot><tr><td colspan="6"></td></tr><tr><td colspan="6"><div><ul><li>total file upload:<span id="numfile" style="margin-right: 10px;padding-left: 5px">0</span></li><li>total file size:<span id="filesize" style="margin-right: 10px;padding-left: 5px">0 b</span></li></ul></div></td></tr></tfoot><tbody id="trUpImgDisplay"></tbody></table></div><div id="container"><button type="button" class="btn-default" id="btnSelect">Select...</button><button type="button" class="btn-primary" id="btnUpload">Upload</button><button type="button" class="btn-warning" id="btnClear">Clear</button></div><div id="console"></div>';
    var tmplRowNoThumb = '<tr id="#=id#"><td class="no-thumbnail"><p>&nbsp;</p></td><td class="text-ellipsis"><p>#=name#</p></td><td><p>#=sizeDisp#</p></td><td class="no-thumbnail"><p>&nbsp;</p></td><td><div class="progress"><div class="progress-bar"></div></div></td><td><p><a href="javascript:void(0)" class="row-remove">remove</a></p></td></tr>';
    var tmplRowThumb = '<tr id="#=id#"><td style="text-align: justify;"><p><a class="thumb-preview" href="javascript:void(0)"><img src="#=fileurl#" class="thumbnail" alt=""/></a></p></td><td class="text-ellipsis"><p>#=name#</p></td><td><p>#=sizeDisp#</p></td><td><p>#=height# x #=width# px. </p></td><td><div class="progress"><div class="progress-bar"></div></div></td><td><p><a href="javascript:void(0)" class="row-remove">remove</a></p></td></tr>';
    /* check silverlight plugin */
    var silverlight = function () {
        try {
            return new ActiveXObject('AgControl.AgControl');
        } catch (e) {
            return navigator.plugins["Silverlight Plug-In"];
        }
    };
    /* check flash plugin */
    var flash = function () {
        try {
            return new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
        } catch (e) {
            return navigator.mimeTypes["application/x-shockwave-flash"];
        }
    };
    /* pluploader instance property */
    this.uploader = function () {
        return uploaderInstance;
    };
    /* initial uploader object */
    this.init = function () {

        //check console support */
        setConsole();

        /* get caller */
        if (elm === undefined || elm === null)
            caller = $('xphotoupload');
        else
            caller = $(elm);

        /* validate caller */
        if (caller == undefined || caller == null || caller.length == 0) {
            throw "cannot initial xPhotoUpload, please varify the container element is already added xphotoupload element and data-* configuration attributes";
        }

        /* clear content
        caller.empty();
        /* initial upload UI */
        caller.html(kendo.template(tmplUploadTable, { useWithBlock: false }));

        /* check support runtime engine */
        if (_URL) {
            caller.find('#tbupload .no-thumbnail').removeClass('no-thumbnail');
            uploadRuntime = 'html5';
        } else if (flash) {
            uploadRuntime = 'flash';
        } else {
            throw "Internet Browser not support, please update to latest version (e.g. Chrome v28, FireFox v22, Opera v16 and IE v10 ) ";
        }

        /* read configurations  */
        this.readConfigurations();

        /* create uploader instance */
        uploaderInstance = new plupload.Uploader({
            runtimes: uploadRuntime,
            max_file_size: sizeLimit,
            chunk_size: chunkSize,
            multi_selection: multiUpload,
            url: urlUpload,
            container: caller.find('#container')[0],
            browse_button: caller.find('#container #btnSelect')[0],
            //silverlight_xap_url: silverlightPath,
            flash_swf_url: flashPath,
            filters: { mime_types: [{ title: "Image files", extensions: allowExts }] }
        });

        this.configConsole(showConsole);

        /* init plupload object */
        uploaderInstance.init();

        uploaderInstance.bind("FilesAdded", function (up, files) {

            if (isAutoUpload && isClearLastUpload) 
                clearUploadFiles();

            plupload.each(files, function (file) {
                var rowContent = '';
                file.sizeDisp = plupload.formatSize(file.size);
                if (_URL) {
                    var img = new Image();
                    img.onload = function () {
                        file.fileurl = this.src;
                        file.width = this.width;
                        file.height = this.height;
                        rowContent = kendo.template(tmplRowThumb)(file);
                        caller.find('#trUpImgDisplay').html(rowContent + caller.find('#trUpImgDisplay').html());
                    };
                    try {
                        img.src = _URL.createObjectURL(file.getNative());
                    } catch (e) {
                        console.log(e.message);
                    }
                    up.refresh(); // Reposition Flash/Silverlight
                } else {
                    rowContent = kendo.template(tmplRowNoThumb)(file);
                    caller.find('#trUpImgDisplay').html(rowContent + caller.find('#trUpImgDisplay').html());
                    up.refresh();
                }
                calculateSummaryInfo();
                up.refresh();

                if (isAutoUpload == true)
                    up.start();

            });
        });

        uploaderInstance.bind("PostInit", function() {
            if (isAutoUpload == false)
                caller.find('#btnUpload').click(function() {
                    if (uploaderInstance.files.length > 0) {
                        uploaderInstance.start();
                    }
                    return false;
                });
            else
                caller.find('#btnUpload').css('display', 'none');
        });

        uploaderInstance.bind("UploadProgress", function (up, file) {
            //console.log(file.percent);
            caller.find('#tbupload tr#' + file.id).find('.progress .progress-bar').css('width', file.percent + '%').html(file.percent + '%');
        });

        uploaderInstance.bind("UploadComplete", function (up, files) {
            try {
                plupload.each(up.files, function (file) {
                    if (file.status == plupload.FAILED) {
                    }
                });
            } catch (e) {
                console.log(e.message);
            }

            // destroy the uploader and init a new one
            if (up.files.length == (up.total.uploaded + up.total.failed)) {
                //display upload file
            }
            //up.destroy();
        });

        uploaderInstance.bind("FileUploaded", function (up, file, info) {
            if (info.response == false) {
                file.status = plupload.FAILED;
                up.trigger('UploadProgress', file);
            }
        });

        uploaderInstance.bind("Error", function (up, err, file) {
            if (!showConsole) return;
            caller.find('#container #console').html("#" + err.code + ": " + err.message + "<br/>");
        });

        /* remove selected upload item */
        caller.on("click", 'a.row-remove', function () {
            var tr = $(this).parents('tr');
            var id = tr.attr('id');
            uploaderInstance.removeFile(uploaderInstance.getFile(id));
            tr.remove();
            calculateSummaryInfo();
        });
        /* remove all upload items */
        caller.on("click", "#container #btnClear", function () {

            caller.find('tbody#trUpImgDisplay tr').each(function () {
                //console.log(row);
                var row = $(this);
                uploaderInstance.removeFile(uploaderInstance.getFile(row.attr('id')));
                row.remove();
            });

            //$('#trUpImgDisplay').empty();
            caller.find('#numfile').html("0");
            caller.find('#filesize').html("0 b");
        });
        /* preview photo on mouse over upload item */
        caller.on('mouseover', 'img.thumbnail', function (e) {
            if (!showPreview) return;
            var id = $(this).parents('tr').attr('id');
            var file = uploaderInstance.getFile(id);
            $(this).popover({
                html: true,
                animation: true,
                placement: function (context, source) {
                    var position = $(source).position();
                    if (position.left > 515) {
                        return "left";
                    }
                    if (position.left < 515) {
                        return "right";
                    }
                    if (position.top < 110) {
                        return "bottom";
                    }
                    return "top";
                },
                title: 'File name: ' + file.name + '<br/>File size : ' + file.sizeDisp,
                //trigger: 'hover',
                content: function () {

                    //var height = file.height > previewHeight ? previewHeight : file.height;

                    return '<img src="' + file.fileurl + '" style="width:100%;" />';
                }
            });
            $(this).popover('show');
            //$('.popover').css('max-width: ' + height + 'px!important;');
        });
        /* remove preview after mouse leave*/
        caller.on('mouseleave', 'img.thumbnail', function () {
            $(this).popover('hide');
            $(this).popover('destroy');
        });
    };

    function clearUploadFiles()
    {
        caller.find('tbody#trUpImgDisplay tr').each(function() {
            //console.log(row);
            var row = $(this);
            uploaderInstance.removeFile(uploaderInstance.getFile(row.attr('id')));
            row.remove();
        });
    }

    /* read configuration from data-* tags */
    this.readConfigurations = function () {
        allowExts = caller.data('filter-extension');
        sizeLimit = caller.data('size-limit');
        chunkSize = caller.data('chunk-size');
        showConsole = caller.data('show-console');
        urlUpload = caller.data('upload-url');
        flashPath = caller.data('flash-url');
        isAutoUpload = caller.data('auto-upload');
        isClearLastUpload = caller.data('auto-upload-clear-last-upload');
        //silverlightPath = caller.data('silverlight-url');
        showPreview = caller.data('show-preview');
        //previewHeight = caller.data('preview-height');
        multiUpload = caller.data('multiple-upload');
    };
    /* set display console */
    this.configConsole = function (disp) {
        if (!disp)
            caller.find('#container #console').hide();
        else
            caller.find('#container #console').show();
        showConsole = disp;
    };
    /* calculate # of file upload and total file size */
    function calculateSummaryInfo() {
        caller.find('#tbupload #numfile').html(uploaderInstance.files.length);
        var totalsize = 0;
        $.each(uploaderInstance.files, function () {
            //console.log(this);
            totalsize += this.size;
        });
        caller.find('#tbupload #filesize').html(plupload.formatSize(totalsize));
    }
    /* mock console object if not available */
    function setConsole() {
        if (!('console' in window)) {
            window.console = {};
        }
        var kind = ['log', 'info', 'warn', 'error', 'assert', 'debug'];
        var stub = function () {; };
        for (var i = 0; i < kind.length; i++) {
            if (kind[i] in window.console) {
                continue;
            }
            window.console[kind[i]] = stub;
        }
    }
    /* call initial uploader method after class initialize */ 
    this.init();
}

//var xpu;
//$(function () {  xpu = new XPhotoUpload(); });