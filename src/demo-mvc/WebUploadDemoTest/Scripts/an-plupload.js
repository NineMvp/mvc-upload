﻿'user strict';

var app = angular.module("fileUploadApp", []);

// utilities shared by all directives
app.factory("appUtil", function () {
    return {
        // watch for changes in scope variables, call update function when all have been initialized
        watchScope: function (scope, props, updateFn, updateOnTimer, updateOnResize) {

            // watch all variables in the props array, keep count of changes and start
            // calling the update function fn only after all variables have been initialized.
            var cnt = props.length;
            angular.forEach(props, function (prop) {
                scope.$watch(prop, function (value) {

                    // decrement count; when this reaches zero, all scope variables are initialized
                    cnt--;

                    // call update function when count reaches zero (all properties have been initialized)
                    // or when the count is negative and the value is non-null.
                    if (cnt == 0 || (cnt < 0 && value)) {
                        console.log(prop + " changing to " + value);
                        if (updateOnTimer) {
                            if (scope.updateTimeout) clearTimeout(scope.updateTimeout);
                            scope.updateTimeout = setTimeout(updateFn, 50);
                        } else {
                            updateFn(prop, value);
                        }
                    }
                })
            });

            // call update function when user resizes the window 
            // so the control can resize itself if it wants to.
            if (updateOnResize) {
                $(window).resize(function () {
                    if (scope.resizeTimeout) clearTimeout(scope.resizeTimeout);
                    scope.resizeTimeout = setTimeout(updateFn, 100);
                })
            }
        }
    }
});

app.directive('browserCap', function ($compile) {
    return {
        restrict: 'A',
        replace: false,
        link: function (scope, ele, attrs) {
            scope.$watch(attrs.browserCap, function (html) {
                ele.html(html);
                $compile(ele.contents())(scope);
            });
        }
    };
});

app.controller('uploadCtlr', function ($scope) {
    //this.uploader;
    $scope.nativeURL = window.URL || window.webkitURL;

    $scope.silverlight = function () {
        try {
            return new ActiveXObject('AgControl.AgControl');
        } catch (e) {
            return navigator.plugins["Silverlight Plug-In"];
        }
    };

    $scope.flash = function () {
        try {
            return new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
        } catch (e) {
            return navigator.mimeTypes["application/x-shockwave-flash"];
        }
    };

    $scope.browserCapa = 'Preview before upload: ' + ($scope.nativeURL != undefined)
        + '<br/>Get Photo dimension: ' + ($scope.nativeURL != undefined)
        + '<br/>Silverlight installed : ' + ($scope.silverlight() != undefined)
        + '<br/>Flash installed : ' + ($scope.flash() != undefined) + '<br/>';

    $scope.UploadFiles = [];
    // [{ id: "444", name: "test", size: 2000000, width: 500, height: 300, fileurl: "http://img.wowebook.com/images/1688426563.jpg", status: false }];


    $scope.removeRowFile = function () {
        //console.log(this);
        var id = angular.element(this).parent().parent().parent().attr("id");
        try {
            $scope.uploader.removeFile($scope.uploader.getFile(id));
            angular.element('#nofile').html(uploader.files.length);
            var totalsize = 0;
            plupload.each($scope.uploader.files, function (file) {
                //console.log(this);
                totalsize += file.size;
            });
            angular.element('#filesize').html(plupload.formatSize(totalsize));

            $scope.uploader.refresh(); // Reposition Flash/Silverlight
        } catch (e) {
        }
        angular.element('tr#' + id).remove();
    };

    $scope.init = function () {
        console.log('Angular Controller Initiallized!');
        //    $scope.uploader.init();
        //    if ($scope.nativeURL)
        //        angular.element('#tbupload .no-html5').removeClass('no-html5');
    };

    angular.element(document).ready(function () {
        $scope.init();
    });


}).directive('mvcUpload', function ($compile) {
    return {
        restrict: 'E',
        replace: true,
        controller: function ($scope) {
            $scope.$watch($scope.UploadFiles, function (newVal) {
                //var loadTrType = '';
                //if (window.URL || window.webkitURL) {
                //    loadTrType = 'GetRowHtml5';
                //} else {
                //    loadTrType = 'GetRowNoHtml5';
                //}

                //console.log(loadTrType);
                //try {
                //    console.log($http);
                //    $http.get(loadTrType, { cache: $templateCache }).success(function(tplContent) {
                //        elem.replaceWith($compile(tplContent)(scope));
                //    });
                //} catch(e) {
                //    console.log(e.message);
                //}
                console.log('mvcUpload controller');
                console.log($scope.UploadFiles);
            }, true);
        },
        templateUrl: 'GetTableTemplate',
        link: function (scope, element, attrs) {

            scope.uploader = new plupload.Uploader({
                runtimes: 'html5,flash,silverlight',
                max_file_size: '10mb',
                chunk_size: '1mb',
                //max_retries: 3,
                //multi_selection: true,
                browse_button: 'btnSelect', // you can pass in id...
                container: element.find('#container')[0], // ... or DOM Element itself
                url: '/UploadFile/Upload',
                multipart_params: {
                    //'AUTHID': auth, //'agoda_ycs_session_token': ycskey
                },
                flash_swf_url: '~/Scripts/plupload/Moxie.swf',
                silverlight_xap_url: '~/Scripts/plupload/Moxie.xap',
                filters: {
                    mime_types: [
                        { title: "Image files", extensions: "jpg,jpeg,png" }
                    ]
                },
                init: {
                    PostInit: function () {
                        //element.find('#trUpImgDisplay').html(''); // = '';
                        element.find('#btnUpload').click(function () {
                            if (scope.uploader.files.length > 0) {
                                scope.uploader.start();
                            }
                            return false;
                        });
                        // $('#console').html(this.uploader.rumtime);
                    },
                    FilesAdded: function (up, files) {
                        //console.log('FilesAdded Fired!');
                        var overSizeLimit = [];
                        plupload.each(files, function (file) {

                            var wfile = { id: file.id, name: file.name, size: plupload.formatSize(file.size), width: 0, height: 0, fileurl: "", status: false };


                            //var sizeStr = plupload.formatSize(file.size);
                            //var filedetail = file.name + ', ' + sizeStr;
                            //var sizes = sizeStr.split(' ');
                            //console.log(filedetail);
                            //if ((sizes[0] > 2 && sizes[1] == "mb") || sizes[1] == "gb") {
                            //    overSizeLimit.push(filedetail);
                            //    up.removeFile(file);
                            //} else
                            if (scope.nativeURL) {
                                var img = new Image();
                                img.onload = function () {
                                    wfile.fileurl = this.src;
                                    wfile.width = this.width;
                                    wfile.height = this.height;

                                    scope.UploadFiles.push(wfile);
                                    //console.log(scope.UploadFiles);

                                };
                                //console.log(_URL);
                                try {
                                    img.src = scope.nativeURL.createObjectURL(file.getNative());
                                } catch (e) {
                                }
                                up.refresh(); // Reposition Flash/Silverlight
                            } else {
                                scope.UploadFiles.push(wfile);
                                //console.log(scope.UploadFiles);
                            }

                            //console.log(wfile);

                            $('#nofile').html(up.files.length);
                            var totalsize = 0;
                            $.each(up.files, function () {
                                //console.log(this);
                                totalsize += this.size;
                            });
                            element.find('#filesize').html(plupload.formatSize(totalsize));
                            up.refresh();
                        });

                        if (overSizeLimit.length > 0) {
                            var overmsg = "<br/><ul>";
                            plupload.each(overSizeLimit, function () {
                                overmsg += '<li>' + this + '</li>';
                            });
                            overmsg += "</ul>";

                        }
                    },
                    UploadProgress: function (up, file) {
                        element.find('tr#' + file.id).find('div#progress').css('width', file.percent / 2);
                        element.find('tr#' + file.id).find('div#progress').html(file.percent + '%');
                    },
                    UploadComplete: function (up, files) {
                        //console.log('UploadComplete Fired!');
                        var failUploadFiles = [];
                        try {
                            plupload.each(up.files, function (file) {
                                if (file.status == plupload.FAILED) {
                                    //console.log('plupload.FAILED !');

                                    var sizeStr = plupload.formatSize(file.size);
                                    failUploadFiles.push(file.name + ', ' + sizeStr);
                                    var td = element.find('#' + file.id).find('td #progress').parent().parent();
                                    //var td = pg.parent().parent();//.parent().hide();
                                    td.empty();
                                    td.width = 350;
                                    td.html('<p  style="font-weight:bold;color:red;">upload failed</p>');
                                }
                            });
                        } catch (e) {
                        } finally {
                        }
                        if (failUploadFiles.length > 0) {
                            var failmsg = "<br/><ul>";
                            $.each(failUploadFiles, function () {
                                failmsg += '<li>' + this + '</li>';
                            });
                            failmsg += "</ul>";
                            // _ycsglobal.MsgBox('error', dimensionMsgErr, failmsg);
                        }

                        // destroy the uploader and init a new one
                        if (up.files.length == (up.total.uploaded + up.total.failed)) {
                            //DisplayNew();
                        }
                        up.destroy();
                    },
                    FileUploaded: function (up, file, info) {
                        //console.log('FileUploaded Fired!');
                        //console.log(info);
                        //console.log(file);
                        if (info.response == false) {
                            file.status = plupload.FAILED;
                            up.trigger('UploadProgress', file);
                        }
                    },
                    Error: function (up, err, file) {
                        angular.element('#console').html("#" + err.code + ": " + err.message + "<br/>");
                    }
                }
            });
            scope.uploader.init();
            if (window.URL || window.webkitURL) {
                element.find('.no-html5').removeClass('no-html5');
            }

        }
    };
}).directive('upload', function ($http, $templateCache, $compile, $parse) {
    //console.log($http);
    return {
        restrict: 'E',
        replace: true,
        scope: {
            uploadModel: '='
        },
        //templateUrl: 'GetRowHtml5',
        link: function (scope, elem, attrs) {

            //scope.$watch(scope.uploadModel, function(newVal) {
            //    console.log('detect changed');
            //    console.log(newVal);
            //}, true);


            //var loadTrType = '';
            //if (window.URL || window.webkitURL) {
            //    loadTrType = 'GetRowHtml5';
            //} else {
            //    loadTrType = 'GetRowNoHtml5';
            //}

            //console.log(loadTrType);
            //try {
            //    console.log($http);
            //    $http.get(loadTrType, { cache: $templateCache }).success(function(tplContent) {
            //        elem.replaceWith($compile(tplContent)(scope));
            //    });
            //} catch(e) {
            //    console.log(e.message);
            //}

            //scope.$watch(attrs.UploadFiles, function(html) {
            //    elem.html(html);
            //    $compile(elem.contents())(scope);
            //});
            //if (parseInt(scope.index) == 0) {
            //    angular.element(attrs.options).css({ 'background-image': 'url(' + scope.item.src + ')' });
            //}
            //elem.bind('click', function() {
            //    var src = elem.find('img').attr('src');
            //    // call your SmoothZoom here
            //    angular.element(attrs.options).css({ 'background-image': 'url(' + scope.item.src + ')' });
            //});
        }
    };
});