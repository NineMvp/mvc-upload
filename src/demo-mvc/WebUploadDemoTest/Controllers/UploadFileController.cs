﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.Mvc;

namespace WebUploadDemoTest.Controllers
{
    public class UploadFileController : Controller
    {
        //
        // GET: /UploadFile/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file)
        {
            //path variable
            var uploadFolderPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "Uploads");
            var uploadDateFolderPath = Path.Combine(uploadFolderPath, DateTime.Now.ToString("yyyyMMdd"));

            //create directory by date
            if (!Directory.Exists(uploadDateFolderPath))
                Directory.CreateDirectory(uploadDateFolderPath);

            //save file upload
            file.SaveAs(Path.Combine(uploadDateFolderPath, file.FileName));
            
            return Json(true);
        }

	}
}